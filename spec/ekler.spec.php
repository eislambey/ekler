<?php
require 'ekler.php';

use function Kahlan\expect;

describe("Example", function() {

    it("test constructor", function() {
      $ekler = new Ekler('emre');
      expect($ekler->yonelme())->toBe("emre'ye");
      expect($ekler->aitlik())->toBe("emre'nin");
      expect($ekler->ayrilma())->toBe("emre'den");
      expect($ekler->belirtme())->toBe("emre'yi");
      expect($ekler->birlikte())->toBe("emre'yle");
    });

    it("test isim method", function(){
      $ekler = new Ekler();
      $ekler->isim("burak");

      expect($ekler->yonelme())->toBe("burak'a");
      expect($ekler->aitlik())->toBe("burak'ın");
      expect($ekler->ayrilma())->toBe("burak'tan");
      expect($ekler->belirtme())->toBe("burak'ı");
      expect($ekler->birlikte())->toBe("burak'la");


    });
});