# Türkçe İsim Ekleri #

PHP 5.4 ve üzeri gerektirir. Ayrıca ***mb_string*** kurulu değilse çalışmayacaktır.

Kullanabileceğiniz metotlar

 * birlikte
 * aitlik
 * yonelme
 * belirtme
 * bulunma
 * ayrilma

Kullanımı

```
$ek = new Ekler('Emre'); 
// opsiyonel olarak iki değer alır. (string)isim ve (boolean)tırnak. 
// otomatik tırnak eklenmesini istemiyorsanız false olarak girmelisiniz 

echo $ek->belirtme(); // Emre'yi 
echo $ek->isim("Sena")->yonelme(); // Sena'ya 
echo $ek->aitlik(); // Sena'nın 
echo $ek->isim('Ufuk', false)->ayrilma(); // Ufuktan

```